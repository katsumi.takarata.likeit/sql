create database practicedb default character set utf8;
use practicedb;

select item_name, item_price
from item
where item_price >= 1000;