create database practicedb default character set utf8;
use practiced

create table item (item_id int PRIMARY KEY auto_increment,
	item_name varchar(256) not null,
	item_price int not null,
	category_id int)