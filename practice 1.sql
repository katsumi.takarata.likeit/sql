create database practicedb default character set utf8;
use practicedb

create table item_category( category_id int primary key auto_increment,
	category_name varchar(256) not null)