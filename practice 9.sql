create database practicedb default character set utf8;
use practicedb;

-- group by できてないやつはselectに書けない！group byとは重複してるカラムの中の値をまとめる
select 
	ic.category_name, sum(i.item_price) as total_price
from 
	item_category ic
left outer join 
	item i
on 
	i.category_id = ic.category_id
	
group by 
	ic.category_id
order by 
	total_price DESC;